<?php

namespace App;

use App\Scopes\TenantScope;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	/**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new TenantScope);
    }

	protected $fillable = ['bank_account','bank_BIC','name','tenant_id'];

	public function transactions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Transaction');
    }
}
