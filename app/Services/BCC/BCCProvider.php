<?php
namespace App\Services\BCC;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

use Illuminate\Support\Arr;


class BCCProvider extends AbstractProvider implements ProviderInterface {

	public function getAuthUrl($state)
	{
		return $this->buildAuthUrlFromBase('https://login.bcc.no/authorize', $state) . "&audience=https%3A%2F%2Fwidgets.brunstad.org";
	}

	public function getTokenUrl()
	{
		return 'https://login.bcc.no/oauth/token';
	}

	public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)],
            'body'    => $this->getTokenFields($code),
        ]);
        return $this->parseAccessToken($response->getBody());
    }

    protected function getTokenFields($code)
    {
        return Arr::add(parent::getTokenFields($code), 'grant_type', 'authorization_code');
    }

    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get('https://login.bcc.no/userinfo', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        return json_decode($response->getBody(), true);
    }

    protected function formatScopes(array $scopes, $scopeSeparator)
    {
        return implode(' ', $scopes);
    }

    protected function mapUserToObject(array $data)
    {
        $data['id'] = explode("|", $data['sub'])[1];
        $data['avatar'] = $data['picture'];
        $data['personId'] = $data['https://login.bcc.no/claims/personId'];
        $data['hasMembership'] = $data['https://login.bcc.no/claims/hasMembership'];
        $data['churchId'] = $data['https://login.bcc.no/claims/churchId'];
        $data['churchName'] = $data['https://login.bcc.no/claims/churchName'];
        $data['app_metadata'] = $data['https://members.bcc.no/app_metadata'];

        unset($data['picture']);
        unset($data['https://login.bcc.no/claims/personId']);
        unset($data['https://login.bcc.no/claims/hasMembership']);
        unset($data['https://login.bcc.no/claims/churchName']);
        unset($data['https://login.bcc.no/claims/churchId']);
        unset($data['https://members.bcc.no/app_metadata']);
        
        
        return (new User)->map($data);
    }

}