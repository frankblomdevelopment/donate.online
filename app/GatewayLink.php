<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayLink extends Model
{
    public $table = 'gateway_link';

    protected $fillable = ['gateway_id','link_id','tenant_id'];
}
