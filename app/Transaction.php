<?php

namespace App;

use App\Customer;
use App\Enums\TransactionStatus;
use App\Gateway;
use App\Link;
use App\Product;
use App\Scopes\TenantScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TenantScope);
    }

    public function scopePaid(Builder $query): Builder
    {
        return $query->where('status', TransactionStatus::Paid);
    }

    public function scopeForGateway(Builder $query, $gateway_id): Builder
    {
        return $query->where('gateway_id', $gateway_id);
    }

    public function scopeForLink(Builder $query, $id): Builder
    {
        return $query->where('link_id', $id);
    }

    public function link(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Link::class)->withTrashed();
    }

    public function gateway(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Gateway::class)->withTrashed();
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function scopeForRange(Builder $builder, Carbon $from, Carbon $to): Builder
    {
        return $builder->whereBetween('created_at', [
            $from->timezone('America/Los_Angeles')->startOfDay(),
            $to->timezone('America/Los_Angeles')->endOfDay()
        ]);
    }

    public function scopeNotEmpty(Builder $builder): Builder
    {
        return $builder->where('amount', '>', 0);
    }

    public function products(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(
            Product::class, 
            'transaction_products',
            'transaction_id',
            'product_id'
        )->withPivot('amount');
    }


}
