<?php

namespace App;

use App\Scopes\TenantScope;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function gateways()
    {
        return $this->hasMany('App\Gateway');
    }

    public function links()
    {
        return $this->hasMany('App\Link');
    }

    public function payments()
    {
        return $this->hasManyThrough('App\Payment','App\Link');
    }
}
