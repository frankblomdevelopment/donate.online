<?php

namespace App;

use App\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed $remainingTickets
 */
class Product extends Model
{
    public function transactions()
    {
        return $this->belongsToMany(
            Transaction::class, 
            'transaction_products',
            'product_id',
            'transaction_id',
        )->withPivot('amount','cost');
    }

    public function hasLimitedAvailability(): bool
    {
        return $this->availability_limit > 0;
    }

    public function getRemainingTicketsAttribute()
    {
        return max($this->availability_limit - $this->tickets(),0);
    }

    public function isAvailable(): bool
    {
        if(!$this->hasLimitedAvailability()) {
            return true;
        }

        return $this->remainingTickets > 0;
    }

    public function count()
    {
        return $this->transactions()->paid()->sum('transaction_products.cost');
    }

    public function scopeForLink(Builder $query, $id)
    {
        return $query->where('link_id', $id);
    }

    public function link()
    {
        return $this->belongsTo(Link::class);
    }

    public function tickets()
    {
        return $this->transactions()->paid()->sum('transaction_products.amount');
    }
    
    public function raised()
    {
        return number_format($this->count(),2,'.',',');
    }
}
