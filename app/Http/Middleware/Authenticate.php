<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class Authenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {

        if (Auth::check()) {
            return $next($request);
        }

        $request->session()->put('customerLogin', false);

        return Socialite::with('BCC')
                ->scopes(['openid','profile','phone','email','church'])
                ->redirect();
    }
}
