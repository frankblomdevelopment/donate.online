<?php

namespace App\Http\Middleware;

use App\Customer;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class IsCustomer
{

        /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if($request->session()->has('customer')){
            return $next($request);
        }

        if (Auth::check()) {
            if($request->session()->has('customer.id')){
                return $next($request);
            }else{
                $customer = Customer::where('person_id', $request->user()->person_id)->first();
                session(['customer' => $customer]);
                return $next($request);
            }
        }

        $request->session()->put('customerLogin_redirect', $request->url());
        $request->session()->put('customerLogin', true);
        
        return Socialite::with('BCC')
                ->scopes(['openid','profile','phone','email','church'])
                ->redirect();
    }
}
