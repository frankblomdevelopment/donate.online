<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTransaction extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gateway' => 'nullable',
            'amount' => 'nullable|numeric|min:1',
            'tip' => 'nullable|numeric|min:1',
            'products' => 'nullable|array',
            'products.*.amount' => 'nullable|numeric|min:0',
            'name' => 'nullable|min:3|max:255'
        ];
    }


    public function attributes()
    {
        return [
            'gateway' => 'recipient',
        ];
    }

    public function messages()
    {
        return [
            'name.required_with' => 'Please fill out the name.',
            'products.*.numeric' => 'Please select a valid amount for this item.',
            'products.*.min' => 'Please select a valid amount for this item.',
            'amount.required_without' => 'Please fill in the amount.'
        ];
    }
}
