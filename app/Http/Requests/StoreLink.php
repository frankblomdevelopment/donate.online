<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLink extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:1',
            'description' => 'required_if:payment_type,fixed',
            'gateways' => 'required',
            'price_type' => 'required',
            'price' => 'nullable',
            'with_tip' => 'boolean',
            'with_tickets' => 'boolean',
            'show_status' => 'boolean',
            'fee' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'gateway' => 'The bank account',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'with_tip' => boolval($this->with_tip),
            'with_tickets' => boolval($this->with_tickets),
            'show_status' => boolval($this->show_status),
            'price' => floatval($this->price),
        ]);
    }
}
