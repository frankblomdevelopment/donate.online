<?php

namespace App\Http\Controllers;

use App\Enums\GatewayType;
use Exception;
use App\Transaction;
use Illuminate\Support\Facades\Request;
use Square\Environment;
use Square\Models\CreatePaymentRequest;
use Square\Models\Money;
use Square\SquareClient;

class PaymentController extends Controller
{
    public function create(Transaction $transaction) {
        switch($transaction->gateway->type) {
            case GatewayType::Stripe:
                return view('transactions.stripe',compact('transaction'));
            case GatewayType::Square:
                return view('transactions.square',compact('transaction'));
            default:
                throw new Exception("No payment provider set");
        };
    }

    public function charge(Transaction $transaction, Request $request)
    {
        $amount_money = new Money();
        $amount_money->setAmount($transaction->amount * 100);
        $amount_money->setCurrency('USD');

        $body = new CreatePaymentRequest(
            $request->get('sourceId'),
            $transaction->id,
            $amount_money
        );

        $client = new SquareClient([
            'accessToken' => getenv('SQUARE_ACCESS_TOKEN'),
            'environment' => Environment::SANDBOX,
        ]);

        $api_response = $client->getPaymentsApi()->createPayment($body);

        if ($api_response->isSuccess()) {
            $result = $api_response->getResult();
        } else {
            $errors = $api_response->getErrors();
        }

        // get frontend has requested payment
    }
}
