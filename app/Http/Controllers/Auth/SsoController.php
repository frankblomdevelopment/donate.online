<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SsoController
{
    protected function signin(User $user, $data)
    {
        $user->person_id = $data->personId;
        $user->email = $data->email;
        $user->name = $data->name;
        $user->save();

        Auth::login($user);

        return redirect('/links');
    }

    protected function signinCustomer(Customer $customer, $data)
    {
        $customer->person_id = $data->personId;
        $customer->email = $data->email;
        $customer->name = $data->name;
        $customer->save();

        session(['customer' => $customer]);
    }
    
    protected function handleCustomerCallback(Request $request, $data)
    {
        $customer = Customer::where('person_id', $data->personId)->orWhere('email', $data->email)->first();
        
        if($customer == null){
            $customer = new Customer();
            $customer->tenant_id = 1;
        }
        
        $this->signinCustomer($customer, $data);
        
        $redirect = $request->session()->get('customerLogin_redirect');
        $request->session()->forget('customerLogin_redirect');
        
        if(!$redirect) return abort(404, "Could not find the link");

        return redirect($redirect);
    }
    
    public function handleProviderCallback(Request $request)
    {
        $data = Socialite::with('BCC')->user();

        if($request->session()->get('customerLogin', false)){
            $request->session()->forget('customerLogin');
            return $this->handleCustomerCallback($request,$data);
        }
        
        $user = User::where('person_id', $data->personId)->first();
        if ($user != null) {
            return $this->signin($user, $data);
        }

        $user = User::where('email', $data->email)->whereNull('person_id')->first();
        if ($user != null) {
            return $this->signin($user, $data);
        }

        return abort(404);
    }


    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();

        return response('You are signed out', 200);
    }
}
