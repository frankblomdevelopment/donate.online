<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Enums\GatewayType;
use App\Enums\LinkDescriptionType;
use App\Enums\LinkPriceType;
use App\Enums\TransactionStatus;
use App\Http\Requests\StoreTransaction;
use App\Link;
use App\Payments\MolliePayment;
use App\Payments\SquarePayment;
use App\Payments\StripePayment;
use App\Transaction;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Spatie\SimpleExcel\SimpleExcelWriter;

class TransactionController extends Controller
{
    private function getTransactions($range) : Builder
    {
        $transactions = Transaction::with('link','customer')
            ->paid()
            ->notEmpty()
            ->orderBy('created_at', 'desc');

        $start = now()->timezone('America/Los_Angeles')->startOfDay();
        $end = now()->timezone('America/Los_Angeles')->endOfDay();

        switch ($range){
            case 'year':
                $transactions->forRange($start->startOfYear(), $end->endOfYear());
                break;
            case 'last_year':
                $transactions->forRange($start->subYear()->startOfYear(), $end->subYear()->endOfYear());
                break;
            case 'quarter':
                $transactions->forRange($start->startOfQuarter(), $end->endOfQuarter());
                break;
            case 'last_quarter':
                $transactions->forRange($start->subQuarter()->startOfQuarter(), $end->subQuarter()->endOfQuarter());
                break;
            case 'month':
                $transactions->forRange($start->startOfMonth(), $end->endOfMonth());
                break;
            case 'last_month':
                $transactions->forRange($start->subMonth()->startOfMonth(), $end->subMonth()->endOfMonth());
                break;
            case 'week':
                $transactions->forRange($start->startOfWeek(), $end->endOfWeek());
                break;
            case 'last_week':
                $transactions->forRange($start->subWeek()->startOfWeek(), $end->subWeek()->endOfWeek());
                break;
            case 'today':
                $transactions->forRange($start, $end);
                break;
            default:
                $transactions->forRange($start->subDays(30), $end);
                break;
        }

        return $transactions;
    }

    public function index()
    {
        $range = request()->input('range', 'l30');

        return view('admin.transactions.index', [
            'transactions' => $this->getTransactions($range)->get(),
            'range' => $range,
            'ranges' => ['l30', 'year','last_year','quarter','last_quarter','month','last_month','week','last_week', 'today'],
        ]);
    }

    public function export(){
        $range = request()->input('range', 'l30');

        $transactions = $this->getTransactions($range)->get();
        $writer = SimpleExcelWriter::streamDownload("Transactions.xlsx");

        $transactions->each(function(Transaction $transaction) use ($writer){
            $writer->addRow([
                'Id' => $transaction->id,
                'PersonId' => $transaction->customer->person_id,
                'Name' => $transaction->customer->name,
                'Link' => $transaction->link->title,
                'Description' => $transaction->description,
                'Amount' => $transaction->amount,
                'Fee' => $transaction->charged_amount,
                'Total' => $transaction->amount + $transaction->charged_amount,
                'Date' => $transaction->created_at->timezone('America/Los_Angeles')->format('m/d/y H:i:s'),
            ]);
        });

       $writer->toBrowser();
    }

    // shown after a payment
    public function show(Request $request, Transaction $transaction){
        $request->session()->flush();

        return view('transactions.show', compact('transaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create(Request $request, Link $link)
    {
        if($link->isExpired()){
            return view('transactions.expired');
        }

        $link->load('gateways');
        $customer = $request->session()->get('customer');

        return view('transactions.form', compact('link','customer'));
    }

    public function history(Request $request, Link $link)
    {
        $customer = $request->session()->get('customer');

        if(!$customer) {
            return "Customer not found";
        }

        $transactions = $customer->transactions()->paid()
            ->forLink($link->id)->with('products')->get();

        return view('transactions.history', compact('link','customer', 'transactions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaction  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreTransaction $request, Link $link)
    {
        $gateways = $link->gateways;
        if($gateways->count() == 1){
            $gateway = $gateways->first();
        }else{
            $gateway = $gateways->find($request->gateway);
        }

        if(is_null($gateway)){
            throw ValidationException::withMessages(['gateway' => "Please select a valid payment method"]);
        }

        if($link->with_tickets && is_null($request->name)){
            throw ValidationException::withMessages(['name' => "Please fill out the name."]);
        }

        $customer = Customer::find($request->session()->get('customer.id'));
        
        if($gateway == null || $customer == null){
            throw ValidationException::withMessages([
                "amount" => ["Something went wrong"], 
            ]);
        }

        // this is a raffle
        if($request->products){

            // look through all products that the link has
            $products = $link->products()->get()->reduce(function($acc, $product) use ($request){
                if($product->multiplier === 0){
                    return $acc;
                }

                // product is not bought
                if(!array_key_exists($product->id, $request->products) || !$request->products[$product->id]){
                    return $acc;
                }

                // product amount is below 1
                if($request->products[$product->id]['amount'] < 1){
                    return $acc;
                }

                // include the multiplier in the sales
                $amount = (int) round($request->products[$product->id]['amount'] * $product->multiplier);

                if ($product->hasLimitedAvailability() && $amount > $product->remainingTickets){
                    throw ValidationException::withMessages([
                        'products.' . $product->id => ["There are only $product->remainingTickets tickets available"],
                    ]);
                }

                $acc[] = [
                    'product_id' => $product->id,
                    'amount' => $amount,
                    'cost' => $amount * $product->price,
                    'multiplier' => $product->multiplier
                ];
                return $acc;
            },[]);

            $amount = collect($products)->sum('cost');
        }else{
            $amount = $link->price_type === LinkPriceType::FIXED() ? $link->price : $request->amount;
            // set products
            $products = null;
        }

        if($link->with_tip && $request->tip > 0){
            $amount+= $request->tip;
        }

        if($amount < 0.50) {
            throw ValidationException::withMessages([
                "amount" => [$request->products ? "Please select at least 1 product" : 'Add an amount'],
            ]);
        }

        $description = ($link->description_type === LinkDescriptionType::FIXED() ? $link->description : $request->input('description'));

        if($description == ""){
            $description = $link->title;
        }

        $transaction = new Transaction();
        $transaction->id = time();
        $transaction->link_id = $link->id;
        $transaction->name = $link->with_tickets ? $request->input('name',null) : $customer->name;
        $transaction->tenant_id = $link->tenant_id;
        $transaction->gateway_id = $gateway->id;
        $transaction->customer_id = $customer->id;
        $transaction->amount = $amount;
        $transaction->charged_amount = $link->fee ? $amount * $link->feeMultiplier() : $amount;
        $transaction->description = $description;
        $transaction->status = TransactionStatus::Open();
        $transaction->save();

        try{
            switch($gateway->type) {
                case GatewayType::Stripe:
                    $payment = new StripePayment($gateway);
                break;
                case GatewayType::Mollie:
                    $payment = new MolliePayment($gateway);
                break;
                case GatewayType::Square:
                    $payment = new SquarePayment($gateway);
                break;
                default:
                    throw new Exception("No payment provider set");
            };
        }catch(\Exception $e){
            Log::info($e);
            return redirect()->route('transaction.error', $transaction);
        }

        $paymentIntend = $payment->create($transaction, $customer);
        $transaction->external_reference = $paymentIntend->id;

        $transaction->save();

        if(!is_null($products)){
            $transaction->products()->attach($products);
        }

        return redirect($paymentIntend->url, 303);
    }

   /**
     * webhook endpoint for sending updates on a transaction
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $provider 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $provider)
    {
        switch($provider) {
            case GatewayType::Stripe:
                return $this->updateStripe($request);
        };
    }
    
    private function updateStripe(Request $request) {
        $transaction = Transaction::with('gateway')->find($request->input('data.object.client_reference_id'));
        if(!$transaction) return;

        return (new StripePayment($transaction->gateway))->update($request, $transaction);
    }

    /**
    * Show error page
    *
    * @param  Transaction  $transaction
    * @return \Illuminate\Http\RedirectResponse
     */
    public function error(Request $request, Transaction $transaction)
    {
        if($transaction->status !== TransactionStatus::Paid()){
            $transaction->status = TransactionStatus::Canceled();
            $transaction->save();
        }

        return redirect()->route('payment-return-url',$transaction);
    }

}
