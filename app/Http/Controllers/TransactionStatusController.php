<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Enums\GatewayType;
use App\Enums\LinkDescriptionType;
use App\Enums\LinkPriceType;
use App\Enums\TransactionStatus;
use App\Http\Requests\StoreTransaction;
use App\Link;
use App\Payments\MolliePayment;
use App\Payments\SquarePayment;
use App\Payments\StripePayment;
use App\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;


class TransactionStatusController extends Controller
{
    public function __invoke(Request $request, Transaction $transaction){
        $transaction->imported = !$transaction->imported;
        $transaction->timestamps = false;
        $transaction->save();

        return redirect()->back();
    }

}
