<?php

namespace App\Http\Controllers;

use App\Gateway;
use Illuminate\Http\Request;
use App\Enums\GatewayType;
use \App\Http\Requests\StoreGateway;


class GatewayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $gateways = Gateway::orderBy('created_at','DESC')->get();
        return view('admin.gateways.index', compact('gateways'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $types = GatewayType::getValues();
        return view('admin.gateways.form',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreGateway  $request
     */
    public function store(StoreGateway $request)
    {
        $gateway =  new Gateway();
        $gateway->name = $request->input('name');
        $gateway->type = $request->input('type');
        $gateway->public_key = $request->input('public_key');
        $gateway->private_key = $request->input('private_key');

        $gateway->save();

        return redirect(route('gateways.index'))->with('status','A new bank account has been created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gateway  $gateway
     */
    public function edit(Gateway $gateway)
    {
        $types = GatewayType::getValues();

        return view('admin.gateways.form', compact(['gateway','types']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreGateway  $request
     * @param  \App\Gateway  $gateway
     */
    public function update(StoreGateway $request, Gateway $gateway)
    {
        $gateway->name = $request->input('name');
        $gateway->type = $request->input('type');
        $gateway->public_key = $request->input('public_key');
        $gateway->private_key = $request->input('private_key');

        $gateway->save();

        return redirect(route('gateways.index'))->with('status','The bank account has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gateway  $gateway
     */
    public function destroy(Gateway $gateway)
    {
        $gateway->delete();

        return redirect(route('gateways.index'))->with('status','The integration has been archived');

    }
}
