<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    
    public function pick(Product $product)
    {
        $names = $product->transactions()->paid()->get()->reduce(function($acc, $transaction) use ($product){
            for($i=0;$i<$transaction->pivot->amount;$i++){
                $acc->push($transaction->name);
            }
            return $acc;
        },collect([]));

            
        return view('products.pick', [
            'product' => $product->load('link'),
            'names' => $names->shuffle()
        ]);
    }
    
}
