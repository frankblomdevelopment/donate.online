<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Enums\LinkExpireType;
use App\Enums\LinkPriceType;
use App\Enums\TransactionStatus;
use App\Gateway;
use App\GatewayLink;
use App\Http\Requests\StoreLink;
use App\Link;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mollie\Laravel\Facades\Mollie;
use Spatie\SimpleExcel\SimpleExcelReader;
use Spatie\SimpleExcel\SimpleExcelWriter;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $links = Link::active()->where('level','<=',$request->user()->level)->orderBy('created_at','DESC')->get();

        return view('admin.links.index', compact('links'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Link  $link
     */
    public function show(Link $link, Request $request)
    {
        return view('admin.links.show',[
            'link' => $link,
            'transactions' => $link->transactions()->paid()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Request $request)
    {
        $gateways = Gateway::where('tenant_id', auth()->user()->tenant_id)->get();

        return view('admin.links.form', compact('gateways'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLink  $request
     */
    public function store(StoreLink $request)
    {

        // create link
        $link = new Link();
        $link->title = $request->title;
        $link->description = $request->description;
        $link->description_type = ($request->description_type == 'fixed' ? "fixed" : 'open');

        $link->price_type = ($request->price_type == 'FIXED' ? LinkPriceType::FIXED() : LinkPriceType::Open());
        $link->price = $request->price;
        $link->with_tip = $request->with_tip;
        $link->with_tickets = $request->with_tickets;

        $link->code = Str::slug($request->title);
        $link->price_type = LinkPriceType::Open();
        $link->expire_type = LinkExpireType::Never();
        $link->fee = $request->fee;

        $link->save();

        $gatewayLinks = [];
        foreach ($request->input('gateways') as $gateway_id) {
            $gatewayLinks[] = [
                'gateway_id' => $gateway_id,
                'link_id' => $link->id,
                'tenant_id' => auth()->user()->tenant_id,
            ];
        }

        // create gateway-link
        GatewayLink::insert($gatewayLinks);

        return redirect(route('links.show',$link))->with('status','A new link is created');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Link  $link
     */
    public function edit(Link $link)
    {
        $gateways = Gateway::where('tenant_id', auth()->user()->tenant_id)->get();
        
        return view('admin.links.form', compact(['gateways','link']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Link  $link
     */
    public function update(StoreLink $request, Link $link)
    {
        $link->title = $request->input('title', $link->title);
        $link->code = Str::slug($link->title);
        $link->fee = $request->input('fee', $link->fee);
        $link->description = $request->input('description');
        $link->description_type = ($request->input('description_type') == 'fixed' ? "fixed" : 'open');

        $link->price_type = ($request->price_type == 'FIXED' ? LinkPriceType::FIXED() : LinkPriceType::Open());
        $link->price = $request->price;
        $link->with_tip = $request->with_tip;
        $link->with_tickets = $request->with_tickets;
        $link->show_status = $request->show_status;

        $gatewayLinks = [];
        foreach ($request->input('gateways') as $gateway_id) {
            $gatewayLinks[$gateway_id] = [
                'link_id' => $link->id,
                'tenant_id' => auth()->user()->tenant_id,
            ];
        }

        $link->gateways()->sync($gatewayLinks);
        $link->save();

        return redirect(route('links.show',$link))->with('status','The link has been updated');
    }

    public function destroy(Link $link)
    {
        $link->delete();
        return redirect(route('links.index'))->with('status','The link has been deleted');
    }

    public function dashboard(Link $link){
        if($link->hasProducts()){
            return view('admin.links.products',compact('link'));
        }else{
            return view('admin.links.dashboard',compact('link'));
        }
    }

    public function export(Link $link){
        $transactions = $link->transactions()->paid()->with('customer')->get();

        $title = Str::slug($link->title,'_') . "_" . now()->timezone('America/Los_Angeles')->format('m-d-y_H:i:s') . '.xlsx';
        $writer = SimpleExcelWriter::streamDownload($title);
        
        $transactions->each(function($transaction) use ($writer){
            $writer->addRow([
                'id' => $transaction->id,
                'PersonId' => $transaction->customer->person_id,
                'Name' => $transaction->customer->name,
                'Email' => $transaction->customer->email,
                'Description' => $transaction->description,
                'Status' => $transaction->status,
                'Amount' => $transaction->amount,
                'Fee' => $transaction->charged_amount,
                'Created' => $transaction->created_at->timezone('America/Los_Angeles')->format('m/d/y H:i:s'),
            ]);
        });
            
        $writer->toBrowser();
    }
    
    public function exportProducts(Link $link){
        $products = $link->products();
        $title = Str::slug($link->title,'_') . "_PRODUCTS_" . now()->timezone('America/Los_Angeles')->format('m-d-y_H:i:s') . '.xlsx';
        $writer = SimpleExcelWriter::streamDownload($title);
        
        $products->each(function($product) use ($writer){
            $product->transactions()->paid()->each(function($transaction) use ($writer, $product){
                for($i=0;$i<$transaction->pivot->amount;$i++){
                    $writer->addRow([
                        'Name' => $transaction->name,
                        'Product' => $product->name,
                    ]);
                }
            });
        });
            
        $writer->toBrowser();
    }
}
