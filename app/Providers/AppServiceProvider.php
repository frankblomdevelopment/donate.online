<?php

namespace App\Providers;

use App\Gateway;
use App\GatewayLink;
use App\Link;
use App\Payment;
use App\User;
use App\Observers\GatewayLinkObserver;
use App\Observers\GatewayObserver;
use App\Observers\LinkObserver;
use App\Observers\PaymentObserver;
use App\Observers\UserObserver;
use App\Services\BCC\BCCProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Http\Request $request)
    {
        Gateway::observe(GatewayObserver::class);
        Link::observe(LinkObserver::class);
        GatewayLink::observe(GatewayLinkObserver::class);

        $this->bootBCCSocialite();

        if ($this->app->isLocal() && $request->server->has('HTTP_X_ORIGINAL_HOST')) {
            $request->server->set('HTTP_HOST', $request->server->get('HTTP_X_ORIGINAL_HOST'));
            $request->headers->set('HOST', $request->server->get('HTTP_X_ORIGINAL_HOST'));
        }
    }

    private function bootBCCSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'BCC',
            function ($app) use ($socialite) {
                $config = $app['config']['services.BCC'];
                return $socialite->buildProvider(BCCProvider::class, $config);
            }
        );
    }
}
