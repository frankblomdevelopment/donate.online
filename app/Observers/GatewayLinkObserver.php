<?php

namespace App\Observers;

use App\GatewayLink;

class GatewayLinkObserver
{
    /**
     * Handle the link "creating" event.
     *
     * @param  \App\Link  $link
     * @return void
     */
    public function creating(GatewayLink $gatewayLink)
    {
        $gatewayLink->tenant_id = auth()->user()->tenant_id;
    }
}
