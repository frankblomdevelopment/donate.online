<?php

namespace App\Observers;

use App\Link;

class LinkObserver
{
    /**
     * Handle the link "creating" event.
     *
     * @param  \App\Link  $link
     * @return void
     */
    public function creating(Link $link)
    {
        $link->tenant_id = auth()->user()->tenant_id;
    }
}
