<?php

namespace App\Observers;

use App\Gateway;

class GatewayObserver
{
    /**
     * Handle the gateway "creating" event.
     *
     * @param  \App\Gateway  $gateway
     * @return void
     */
    public function creating(Gateway $gateway)
    {
        $gateway->tenant_id = auth()->user()->tenant_id;
    }
}
