<?php

namespace App\Payments;

use App\Customer;
use App\Payment;
use \Mollie\Api\MollieApiClient;
use Illuminate\Http\Request;


class MolliePayment
{
    private $api;
    private $gateway;

    public function __construct(Gateway $gateway){
        $this->gateway = $gateway;
        $this->api = new MollieApiClient();
        $this->api->setApiKey($gateway->private_key);

        return $this;
    }

    /**
     *
     * @param  \App\Transaction  $transaction
     * @param  \App\Customer $customer
     * @return  \App\Payment\PaymentIntent 
     */
    public function create(Transaction $transaction, Customer $customer){

        $payment = $this->api->payments()->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => number_format($transaction->amount,2,'.',''),
            ],
            'locale' => 'nl_NL',
            'method' => 'ideal',
            'description' => $transaction->description,
        ]);

        return new PaymentIntent($payment->id, $payment->getCheckoutUrl());
    }

    public function update(Request $request, Transaction $transaction){
        $payment = $this->api->payments()->get($transaction->external_reference);

        // update the transaction status
        $transaction->status = TransactionStatus::getInstance($payment->status);
        if($transaction->status->is(TransactionStatus::Paid())){
            //get the name from the cache
            // make sure to bind the new customer
            $customer = Customer::updateOrCreate(array(
                'bank_account' => $payment->details->consumerAccount,
                'tenant_id' => $transaction->tenant_id,
                'bank_BIC' => $payment->details->consumerBic,
            ),array(
                'name' => $payment->details->consumerName,
            ));

            $transaction->customer_id = $customer->id;
        }


        $transaction->save();
    }
}
