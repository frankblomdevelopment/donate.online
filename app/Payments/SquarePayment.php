<?php

namespace App\Payments;

use App\Customer;
use App\Enums\TransactionStatus;
use App\Gateway;

use App\Transaction;
use Illuminate\Http\Request;
use Stripe\StripeClient;


class SquarePayment
{
    private $api;
    private Gateway $gateway;

    public function __construct(Gateway $gateway){
        $this->gateway = $gateway;
        $this->api = new StripeClient($gateway->private_key);
        return $this;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Transaction  $transaction 
     * @param  \App\Customer  $customer 
     * @return  PaymentIntent
     */
    public function create(Transaction $transaction, Customer $customer)
    {
        return new PaymentIntent($transaction->id, route('payment.create', $transaction));
    }

    public function update(Request $request, Transaction $transaction){
//        switch($request->type){
//            case "checkout.session.async_payment_failed":
//                $transaction->status = TransactionStatus::Failed();
//            break;
//            case "checkout.session.completed":
//            case "checkout.session.async_payment_succeeded":
//                $transaction->status = TransactionStatus::Paid();
//            break;
//        }

        $transaction->save();
    }
}
