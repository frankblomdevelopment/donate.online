<?php 

namespace App\Payments;

class PaymentIntent {
    public $id;
    public $url;

    function __construct($id, $url) {
        $this->id = $id;
        $this->url = $url;
    }
}