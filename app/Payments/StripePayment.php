<?php

namespace App\Payments;

use App\Customer;
use App\Enums\TransactionStatus;
use App\Gateway;

use App\Transaction;
use Illuminate\Http\Request;
use Stripe\StripeClient;


class StripePayment
{
    private $api;
    private $gateway;

    public function __construct(Gateway $gateway){
        $this->gateway = $gateway;
        $this->api = new StripeClient($gateway->private_key);
        return $this;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Transaction  $transaction 
     * @param  \App\Customer  $customer 
     * @return  \App\Transaction\PaymentIntent 
     */
    public function create(Transaction $transaction, Customer $customer)
    {
        $stripeCustomer = $this->getStripeCustomer($customer);

        $items = array(
            array(
                'description' =>  $transaction->description,
                'price_data' => [
                    'currency' => 'USD',
                    'unit_amount' => number_format($transaction->amount,2,'',''),
                    'product_data' => [
                        'name' => 'Donation',
                    ],
                ],
                'quantity' => 1,
            ),
        );

        if($transaction->link->fee){
            array_push($items, array(
                'description' =>  $transaction->link->fee . "% Transaction Fee",
                'price_data' => [
                    'currency' => 'USD',
                    'unit_amount' => number_format($transaction->amount * $transaction->link->feeMultiplier(), 2,'',''),
                    'product_data' => [
                        'name' => $transaction->link->fee . "% Transaction Fee",
                    ],
                ],
                'quantity' => 1,
            ));
        }

        $intent = $this->api->checkout->sessions->create([
            'cancel_url' => route('transaction.error', $transaction->id),
            'success_url' => route('payment-return-url', $transaction),
            'client_reference_id' => $transaction->id,
            'customer' => $stripeCustomer->id,
//            'payment_method_types' => ['card'],
            'metadata' => ['person_id' => $customer->person_id],
            'line_items' => $items,
            'mode' => 'payment',
          ]);
        
        return new PaymentIntent($intent->id, route('payment.create', $transaction));
    }

    protected function getStripeCustomer(Customer $customer){
        if($customer->stripe_id != null){
            $stripeCustomer = $this->api->customers->retrieve($customer->stripe_id);

            if($stripeCustomer != null && !$stripeCustomer->deleted){
                return $stripeCustomer;
            }
        }

        $stripeCustomer = $this->api->customers->create([
            'email' => $customer->email,
            'name' => $customer->name,
            'metadata' => ['person_id' => $customer->person_id],
        ]);

        $customer->stripe_id = $stripeCustomer->id;
        $customer->save();

        return $stripeCustomer;
    }

    public function update(Request $request, Transaction $transaction){
        switch($request->type){
            case "checkout.session.async_payment_failed":
                $transaction->status = TransactionStatus::Failed();
            break;
            case "checkout.session.completed":
            case "checkout.session.async_payment_succeeded":
                $transaction->status = TransactionStatus::Paid();
            break;
        }

        $transaction->save();
    }
}
