<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Prefilled()
 * @method static static PrefilledMultiple()
 * @method static static Open()
 * @method static FIXED()
 */
final class LinkPriceType extends Enum
{
    const Prefilled = 'prefilled';
    const PrefilledMultiple = 'prefilled-multiple';
    const Open = 'open';
    const FIXED = 'FIXED';
}
