<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Never()
 * @method static static ByDate()
 */
final class LinkExpireType extends Enum
{
    const Never = 'never';
    const ByDate = 'by-date';
}
