<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Open()
 * @method static static Canceled()
 * @method static static Pending()
 * @method static static Authorized()
 * @method static static Expired()
 * @method static static Failed()
 * @method static static Paid()
 */
final class TransactionStatus extends Enum
{
    const Open = 'open';
    const Canceled = 'canceled';
    const Pending = 'pending';
    const Authorized = 'authorized';
    const Expired = 'expired';
    const Failed = 'failed';
    const Paid = 'paid';
}
