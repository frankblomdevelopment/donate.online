<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Mollie()
 * @method static static Stripe()
 * @method static static Square()
 */
final class GatewayType extends Enum
{
    const Mollie = 'mollie';
    const Stripe = 'stripe';
    const Square = 'square';
}
