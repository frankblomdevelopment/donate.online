<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static TransactionInit()
 * @method static static TransactionUpdate()
 */
final class MessageType extends Enum
{
    const TransactionInit = 'transaction-init';
    const TransactionUpdate = 'transaction-update';
}
