<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static FIXED()
 * @method static static OPEN()
 */
final class LinkDescriptionType extends Enum
{
    const FIXED = 'fixed';
    const OPEN = 'open';
}
