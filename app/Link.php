<?php

namespace App;

use App\Enums\LinkExpireType;
use App\Enums\TransactionStatus;
use App\Gateway;
use App\Product;
use App\Scopes\TenantScope;
use App\Tenant;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TenantScope);
    }

    public $casts = [
        'with_tip' => 'boolean',
        'with_tickets' => 'boolean',
        'show_status' => 'boolean',
    ];

    public function getRouteKeyName()
    {
        return 'code';
    }

    public function scopeActive(Builder $query){
        return $query
            ->where('expire_type', new LinkExpireType(LinkExpireType::Never))
            ->orWhere(function($query){
                $query->where([
                    ['expire_type','=', new LinkExpireType(LinkExpireType::ByDate)],
                    ['expiration_date','>',Carbon::now()]
                ]);
            });
    }

    public function scopeExpired(Builder $query){
        return $query->where('expire_type', new LinkExpireType(LinkExpireType::ByDate))
            ->where('expiration_date','<',Carbon::now());
    }

    public function isExpired(): bool
    {
        if($this->expire_type == 'never') return false;
        if(Carbon::create($this->expiration_date)->isFuture()) return false;
        return true;
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function gateways()
    {
        return $this->belongsToMany(Gateway::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class)->orderBy('updated_at','DESC');
    }
    
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function hasProducts(): bool
    {
        return $this->products->count() > 0;
    }

    public function hasTips() : bool
    {
        return $this->with_tip;
    }

    public function getProductSalesAttribute()
    {
        $this->transactions()->paid()
            ->join('transactions_products','transactions.id','=','transactions_products.transaction_id')
            ->sum('transactions_products.cost');

//        return $this->transactions()->transactions()->paid()->sum('cost');
    }

    public function feeMultiplier()
    {
        return $this->fee / 100;
    }

    public function getRaisedAttribute(): string
    {
        return number_format($this->transactions()->paid()->sum('transactions.amount'),2);
    }

    public function paidCustomers()
    {
        return $this->transactions()->paid()->groupBy('transactions.customer_id');
    }

    public function raisedForGateway(Gateway $gateway): string
    {
        return number_format($this->transactions()->paid()->forGateway($gateway->id)->sum('transactions.amount'),2,'.',',');
    }
    
    public function raisedForProduct(Product $product): string
    {
        return number_format($this->products()->where('id',$product->id)->transactions()->paid()->sum('transactions.amount'),2,'.',',');
    }

    public function raisedForProducts(): string
    {
        return number_format($this->transactions()->paid()
            ->join('transaction_products','transactions.id','=','transaction_products.transaction_id')
            ->sum('transaction_products.cost'), 2,'.',',');
    }

    public function raisedDonations(): string
    {
        $product_transactions =  $this->transactions()->paid()
            ->join('transaction_products','transactions.id','=','transaction_products.transaction_id')
            ->sum('transaction_products.cost');

        $total = $this->transactions()->paid()->sum('transactions.amount');

        return number_format($total - $product_transactions,2,'.',',');
    }


    public function participantsForGateway(Gateway $gateway){
        return $this->transactions()->forGateway($gateway->id)->paid()->distinct('transactions.customer_id');
    }

    public function transactionsForGateway(Gateway $gateway){
        return $this->transactions()->forGateway($gateway->id)->paid();
    }
}
