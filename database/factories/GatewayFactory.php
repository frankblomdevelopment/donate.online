<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\GatewayType;
use App\Gateway;
use Faker\Generator as Faker;

$factory->define(Gateway::class, function (Faker $faker) {
    return [
        'tenant_id' => 1,
        'name' => $faker->name,
        'type' => GatewayType::Mollie(),
        'api_key' => "test_PG6dUSkN32Uz8ammCgzCzWjHpPvsKg",
    ];
});
