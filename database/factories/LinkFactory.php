<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\LinkExpireType;
use App\Enums\LinkPriceType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'tenant_id' => 1,
        'title' => $faker->name,
        'code' => Str::random(12),
        'tenant_id' => 1,
        'price_type' => LinkPriceType::Open(),
        'expire_type' => LinkExpireType::ByDate(),
        'expiration_date' => $faker->dateTimeThisYear('2020-01-01'),
        'price' => $faker->randomNumber(2),
    ];
});
