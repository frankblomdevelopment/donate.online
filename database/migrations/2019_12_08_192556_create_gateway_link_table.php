<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGatewayLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_link', function (Blueprint $table) {
            $table->bigInteger('tenant_id')->unsigned();
            $table->bigInteger('gateway_id')->unsigned();
            $table->bigInteger('link_id')->unsigned();
        });

        Schema::table('gateway_link', function (Blueprint $table) {
            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('gateway_id')->references('id')->on('gateways');
            $table->foreign('link_id')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways_links');
    }
}
