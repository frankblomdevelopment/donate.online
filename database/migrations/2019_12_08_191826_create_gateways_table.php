<?php

use App\Enums\GatewayType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateways', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tenant_id')->unsigned();
            $table->string('name');
            
            $table->enum('type', GatewayType::getValues());
            
            $table->string('public_key', 200);       
            $table->string('private_key', 200);       
                        
            $table->timestampsTz();
            $table->softDeletesTz();
            
        });

        Schema::table('gateways', function (Blueprint $table) {
            $table->foreign('tenant_id')->references('id')->on('tenants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways');
    }
}
