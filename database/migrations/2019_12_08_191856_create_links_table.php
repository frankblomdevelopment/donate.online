<?php

use App\Enums\LinkExpireType;
use App\Enums\LinkPriceType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tenant_id')->unsigned();
            $table->string('title');
            $table->string('code');
            $table->enum('price_type', LinkPriceType::getValues())->default(LinkPriceType::Open());
            $table->enum('expire_type', LinkExpireType::getValues())->default(LinkExpireType::Never());
            $table->timestampTz('expiration_date')->nullable();
            $table->double('price')->nullable();
            $table->timestampsTz();
            $table->softDeletesTz();
        });
        
        Schema::table('links', function (Blueprint $table) {
            $table->foreign('tenant_id')->references('id')->on('tenants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
