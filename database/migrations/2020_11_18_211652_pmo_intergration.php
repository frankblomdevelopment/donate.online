<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PmoIntergration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->string('person_id',50)->nullable();
        });
        
        Schema::table('customers', function($table){
            $table->string('person_id',50)->nullable();
        });

        Schema::table('tenants', function ($table) {
            $table->integer('church_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
