<?php

use App\Enums\TransactionStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tenant_id')->unsigned();
            $table->bigInteger('link_id')->unsigned();
            $table->bigInteger('gateway_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned()->nullable();
            
            $table->string('external_reference');
            $table->string('description');
            $table->double('amount');
            $table->enum('status', TransactionStatus::getValues())->default(TransactionStatus::Open());
            $table->timestampsTz();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('link_id')->references('id')->on('links');
            $table->foreign('gateway_id')->references('id')->on('gateways');
            $table->foreign('tenant_id')->references('id')->on('tenants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
