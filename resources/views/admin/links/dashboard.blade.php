@extends('layouts.public')

@section('meta')
	<meta http-equiv="refresh" content="8">
@stop

@section('content')
	<h1 class="w-full text-center text-4xl py-4">{{$link->title}}</h1>
	<div class="absolute w-full h-full top-0 text-center">
		<span id="counter">$ {{ $link->raised }}</span>
	</div>
	<style type="text/css">
		#counter {
			font-size: 180px;
			line-height: 100vh;
		}
	</style>
@stop

