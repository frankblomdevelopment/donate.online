@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 ">
        @if (session('status'))
            <div class="text-sm border rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
        @endif
            @if(auth()->user()->level > 2)
                <div class="w-full flex justify-center items-start space-x-2 mb-4">
                    <div class="flex space-x-2">
                        <a class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-4 px-5 rounded text-gray-800 mr-2" href="{{route('links.edit',$link)}}">Edit</a>
                        <a class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-4 px-5 rounded text-gray-800 mr-2" href="{{route('links.export',$link)}}">Transactions</a>
                        @if($link->hasProducts())<a class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-4 px-5 rounded text-gray-800" href="{{route('links.products.export',$link)}}">Product Export</a>@endif
                    </div>
                    <div class="flex ml-auto">
                        <form method="POST" action="{{route('links.destroy',$link)}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="ml-auto bg-red-200 hover:bg-red-100 border border-red-400 inline-block py-4 px-5 rounded text-red-800">Delete</button>
                        </form>
                        <a class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-4 px-5 rounded text-gray-800 ml-2" href="{{route('links.dashboard',$link)}}">Live status</a>
                    </div>
                </div>
            @endif
        <div class="w-full mb-6 mx-auto md:p-0 lg:w-3/4 xl:w-2/3 block float-left relative">
            <div class="flex flex-col break-words bg-white border rounded shadow-md py-6">
                <h1 class="px-6 text-xl mb-6 text-center">Link for <b>{{$link->title}}</b></h1>
                <div class="mb-6 px-6">
                    <span class="font-medium w-full block mb-2">Shareable link</span>
                    <div class="flex mt-2">
                        <div class="w-full px-4 py-4 border border-size-1 border-gray-400 bg-yellow-100 rounded text-xl text-gray-800 shadow-inner truncate">
                            <a href="{{route('transaction.create', $link->code)}}">{{route('transaction.create', $link->code)}}</a>
                        </div>
                        <a class="bg-blue-500 hover:bg-blue-400 text-white border border-blue-600 inline-block ml-2 py-4 px-6 text-xl rounded" href="{{route('transaction.create', $link->code)}}" target="_blank">Open</a>
                    </div>
                </div>
                <div class="overflow-x-auto w-full">
                    <table class="mb-4 w-full">
                        <tr class="bg-gray-100 border-b-2 border-t-2">
                            @if(auth()->user()->level > 2)
                            <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left" width="15%">Amount</th>
                            @endif
                            <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left">Name</th>
                                @if(auth()->user()->level > 3)
                            <th class="px-0 py-3 uppercase text-sm text-gray-500 text-left" >Description</th>
                                @endif
                            @if($link->gateways->count() > 1)
                                <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left">Account</th>
                            @endif
                            <th class="px-6 py-3 uppercase text-sm text-gray-500 text-right">Date</th>
                            <th></th>
                        </tr>
                    @forelse($transactions as $transaction)
                        <tr class="border-b">
                            @if(auth()->user()->level > 2)
                            <td class="px-6 py-4 font-bold">${{number_format($transaction->amount,2,'.',',')}}</td>
                            @endif
                            <td class="px-6 py-4 text-green-600">{{($transaction->customer->name ?? '' )}}</td>
                            @if(auth()->user()->level > 3)
                            <td class="px-0 py-4">
                                {{ $transaction->description }}
                            </td>
                            @endif
                            @if($link->gateways->count() > 1)
                                <td class="px-6 py-4">{{($transaction->gateway->name ?? '' )}}</td>
                            @endif
                            <td class="px-6 py-4 text-right text-gray-600" title="{{$transaction->updated_at->timezone('America/Los_Angeles')->diffForHumans()}}">{{$transaction->updated_at->timezone('America/Los_Angeles')->format('d M H:i')}}</td>
                            <td>
                                <a href="{{route('transactions.status.toggle', $transaction)}}" class="px-2 py-1">
                                    @if($transaction->imported)
                                        ✅
                                    @else
                                        🚫
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" class="py-4 with-full text-center text-gray-500">No transactions have been made yet</td></tr>
                    @endforelse
                    </table>
                </div>
            </div>
        </div>
        <div class="w-full mb-6 mx-auto md:p-0 lg:pl-4 lg:w-1/4 xl:w-1/3 block float-left">
            <div class="bg-white border rounded shadow-md p-4 mb-4">
                <h2 class="text-xl text-gray-600 mb-4 text-center">Total</h2>
                <div class="flex">
                    @include('admin.partials.infoblock',['data' => ['Recieved' => "$". $link->raised, 'Transactions' => $link->transactions()->paid()->count()]])
                </div>
            </div>
            @if($link->gateways->count() > 1)
            <div class="bg-white border rounded shadow-md p-4 mb-4">
                @foreach($link->gateways as $gateway)
                    <span class="block font-medium w-full block mb-2 text-center text-gray-600">{{$gateway->name}}</span>
                    <div class="flex w-full pt-4 pb-8 last:pb-4 mb-4 last:mb-0 border-b last:border-b-0">
                        @include('admin.partials.infoblock',['data' => ['Recieved' => "$". $link->raisedForGateway($gateway), 'Transactions' => $link->participantsForGateway($gateway)->count() ]])
                    </div>
                @endforeach
            </div>
            @endif
            @if($link->hasProducts())
            <div class="bg-white border rounded shadow-md p-4 mb-4">
                <h2 class="text-xl text-gray-600 mb-4 text-center">Products</h2>
                @foreach($link->products()->where('multiplier','>',0)->orderBy('products.order')->get() as $product)
                <div class="flex flex-wrap border-b last:border-b-0 py-2">
                    <a href="{{route('product.pick',$product)}}" class="block font-medium w-full block text-center text-gray-600">{{$product->name}}</a>
                    <div class="flex w-full pt-4 pb-2  ">
                        @include('admin.partials.infoblock',['data' => ['Recieved' => "$". $product->raised(),'Sold' => $product->tickets()]])
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            @if($link->hasTips())
                <div class="bg-white border rounded shadow-md p-4 mb-4">
                    <h2 class="text-xl text-gray-600 mb-4 text-center">Donations</h2>
                    <div class="flex w-full pt-4 pb-2  ">
                        @include('admin.partials.infoblock',['data' => ['Total' => "$". $link->raisedDonations()]])
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
