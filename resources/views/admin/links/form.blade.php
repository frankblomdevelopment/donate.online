@extends('layouts.app')

@section('content')
    <div class="flex items-center">
        <div class="w-full sm:w-full px-4 md:mx-auto md:p-0 max-w-3xl">

            @if (session('status'))
                <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="flex justify-between mb-5">
                <h1 class="font-medium text-gray-500 pt-4">
                    @if(isset($link))
                        Edit Link
                    @else
                        Create new Link 
                    @endif
                </h1>
            </div>
            <div class="flex flex-col break-words bg-white border-2 rounded shadow-md">
                <form method="post" action="{{(isset($link) ? route('links.update', $link) : route('links.store') )}}" class="px-6 pt-10 pb-6">
                    @if(isset($link)) @method('put') @endif
                    @csrf
                    <div class="mb-6">
                        <span class="font-medium w-full block mb-2">Title</span>
                        <input value="@if(isset($link)){{$link->title}}@else{{old('title')}}@endif" class="text-xl border p-4 block w-full rounded bg-gray-100" type="text" name="title" placeholder="This link is for ...">
                        @error('title')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                    <div class="mb-8">
                        <span class="font-medium w-full block mb-2">Description</span>
                        <textarea class="text-xl border p-2 block w-full rounded bg-gray-100" name="description">@if(isset($link)){{$link->description}}@else {{ old('description') }}@endif</textarea>
                        @error('description')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror

                        <label class="inline-flex cursor-pointer w-full py-2 border-gray-200">
                            <input type="checkbox" class="form-checkbox" name="description_type" value="fixed" @if(isset($link) && $link->description_type == "fixed") checked="checked" @endif>
                            <span class="ml-2">Show this description on the invoice for every donation.</span>
                        </label>
                    </div>
                    <div class="flex w-full mb-6">
                        <div class="w-full flex">
                            <div class="w-full">
                                <span class="font-medium w-full block mb-2">Price</span>
                                <div class="flex relative w-24">
                                    <select name="price_type" class="p-3 border rounded ">
                                        <option value="Open" @if((isset($link) && $link->price_type == 'Open') || old('price_type') == 'Open') selected @endif">Open</option>
                                        <option value="FIXED" @if((isset($link) && $link->price_type == 'FIXED') || old('price_type') == 'FIXED') selected @endif">Fixed</option>
                                    </select>
                                </div>
                                @error('price_type')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                            </div>
                            <div class="w-full">
                                <span class="font-medium w-full block mb-2">Price</span>
                                <div class="flex relative w-32">
                                    <span class="text-2xl text-gray-500 font-mono absolute pt-3 px-5 left-0">$</span>
                                    <input value="@if(isset($link)){{$link->price}}@else{{old('price','0')}}@endif" class="pl-10 py-3 text-xl text-gray-600 font-mono tracking-tighter rounded-lg border w-full outline-none text-left" step="0.1" type="number" name="price">
                                </div>
                                @error('description')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                            </div>
                        </div>

                        <div class="w-full">
                            <span class="font-medium w-full block mb-2">Processing fee</span>
                            <div class="flex relative w-24">
                                <input value="@if(isset($link)){{$link->fee}}@else{{old('fee','3')}}@endif" class="pr-8 pl-3 py-3 text-xl text-gray-600 font-mono tracking-tighter rounded-lg border w-full outline-none text-center" type="number" name="fee">
                                <span class="text-2xl text-gray-500 font-mono absolute pt-3 px-5 right-0">%</span>
                            </div>
                            @error('fee')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                        </div>

                    </div>
                    <div class="w-1/2 mb-6">
                        <span class="font-medium w-full block mb-2">Options</span>
                        <label class="inline-flex cursor-pointer block w-full py-2 border-gray-200">
                            <input type="checkbox" class="form-checkbox" name="with_tip" value="true" @if(isset($link) && $link->with_tip) checked="checked" @endif>
                            <span class="ml-2">Allow Tipping</span>
                        </label>
                        <label class="inline-flex cursor-pointer block w-full py-2 border-gray-200">
                            <input type="checkbox" class="form-checkbox" name="with_tickets" value="true" @if(isset($link) && $link->with_tickets) checked="checked" @endif>
                            <span class="ml-2">Sell Tickets</span>
                        </label>
                        <label class="inline-flex cursor-pointer block w-full py-2 border-gray-200">
                            <input type="checkbox" class="form-checkbox" name="show_status" value="true" @if(isset($link) && $link->show_status) checked="checked" @endif>
                            <span class="ml-2">Show Status on the link page</span>
                        </label>
                        @error('with_tip')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                        @error('with_tickets')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                        @error('show_status')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                    <div class="mb-6">
                        <span class="font-medium w-full block mb-2">Payment Providers:</span>
                        @foreach($gateways as $gateway)
                            <label class="inline-flex cursor-pointer w-full py-2 border-gray-200">
                                <input type="checkbox" class="form-checkbox" name="gateways[]" value="{{$gateway->id}}" @if(isset($link) && $link->gateways->contains('id',$gateway->id)) checked="checked" @endif>
                                <span class="ml-2">{{$gateway->name}}</span>
                            </label>
                        @endforeach
                        @error('gateways')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                    <hr class="mb-6">
                    <button class="bg-blue-500 hover:bg-blue-400 border border-blue-600 text-white py-3 px-4 rounded inline-block float-right" type="submit">@if(isset($link)) Save @else Create @endif</button>

                    <a href="{{(isset($link) ? route('links.show',$link) : route('links.index'))}}" class="float-right py-3 mx-4 text-gray-700">Cancel</a>
                </form>
            </div>
        </div>
    </div>
@endsection
