@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        @if (session('status'))
            <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <div class="w-full block float-left relative">
           <div class="absolute w-full flex justify-between">
                <h1 class="font-medium text-gray-500 pt-4">
                    All Links
                </h1>
                <a class="bg-blue-500 hover:bg-blue-400 text-white border border-blue-600 inline-block py-4 px-5 rounded" href="{{route('links.create')}}">New Link</a>
            </div>
            <table class="bg-white border rounded shadow-md py-6 mt-16 w-full">
                <tr class="bg-gray-100 border-b-2">
                    <th class="px-6 py-4 uppercase text-sm text-gray-500 text-left">Name</th>
                    <th class="uppercase text-sm text-gray-500 text-left">Status</th>
                    <th class="px-6 py-4 uppercase text-sm text-gray-500 text-left">Recieved</th>
                    <th class="px-6 py-4 uppercase text-sm text-gray-500 text-center">Participants</th>
                    <th class=""></th>
                </tr>

            @foreach($links as $link)
                <tr class="border-b">
                    <td class="px-6 py-6"><a class="" href="{{route('links.show',$link)}}">{{$link->title}}</a></td>
                    <td class=""><span class="py-2 px-3 rounded text-green-700 text-sm font-bold bg-green-200">Open</span></td>
                    <td class="px-6 py-6 font-bold">$ {{$link->raised}}</td>
                    <td class="px-6 py-6 font-bold text-center">{{$link->paidCustomers->count()}}</td>
                    <td class="text-right pr-2">
                        <a href="{{route('links.show',$link)}}" class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-3 px-4 rounded text-gray-800">Show</a>
                    </td>
                </tr>
            @endforeach
            </table>
        </div>
    </div>
@endsection
