@extends('layouts.public')

@section('meta')
	<!-- <meta http-equiv="refresh" content="8"> -->
@stop

@section('content')
	<h1 class="w-full text-5xl py-6 text-center font-bold">🎉 {{$link->title}} 🎉</h1>
	<div class="max-w-2xl mt-4 mx-auto bg-white w-full  border rounded p-4">
		@foreach($link->products()->orderBy('products.order')->get() as $product)
			<a href="{{route('product.pick',$product)}}" class="w-full flex flex-wrap items-center mb-8">
				<div class="flex w-full justify-between items-center">
					<span class="font-medium text-4xl py-2">{{$product->name}}</span>
					<span class="text-right text-3xl"><b>{{$product->tickets()}}</b> <small>tickets</small></span>
				</div>
				<div class="w-full product" data-product="{{$product->id}}">
				</div>
			</a>
		@endforeach
	</div>

	<style>
		body {
			background-color: #e5dbdb;
			background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ac9292' fill-opacity='0.4'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");
		}
		
		.ticket {
			outline: 12px dotted #f7fafc;
    		outline-offset: -5px;
		}
	</style>

	<script>
		function getCookie(cName) {
			const name = cName + "=";
			const cDecoded = decodeURIComponent(document.cookie); //to be careful
			const cArr = cDecoded .split('; ');
			let res;
			cArr.forEach(val => {
				if (val.indexOf(name) === 0) res = val.substring(name.length);
			})
			return res;
		}

		const products = document.getElementsByClassName('product');

		for(let i=0;i<products.length;i++){
			const p = products[i];
			const id = p.dataset.product;
			const winner = getCookie(id);
			if(winner){
				p.innerHTML = "<span class=\"text-green-600 text-3xl font-bold\">🎉 "+winner+" </span>"
			}
		}
	</script>

	
@stop

