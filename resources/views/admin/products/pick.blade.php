@extends('layouts.public')

@section('script')
	<script src="\js\confetti.js"></script>
@stop
@section('content')
	<div id="tickets" class="flex flex-wrap w-full p-12 justify-center">
		@foreach($names as $name)
		<div class="ticket flex py-5 px-4 bg-red-500 m-1" data-name="{{$name}}">
			<div class="inner w-full flex flex-wrap py-3 px-4 border-2 border-red-800 rounded bg-red-500">
				<h1 class="w-full font-bold text-red-900">{{$name}}</h1>
			</div>
		</div>
		@endforeach
	</div>
	<div id="overlay" class="flex justify-center items-center">
		<button id="pickbtn" class="bg-green-500 text-5xl border-2 px-12 py-6 rounded shadow-lg text-green-900 border-green-600" onclick="pick()">The Winner for the <b>{{$product->name}}</b> is...</button>
	</div>
	<div id="next" class="w-full items-center justify-center flex p-4 hidden">
		<a href="{{route('links.dashboard',$product->link->code)}}" class="text-3xl bg-blue-200 border font-bold p-4 rounded-lg border-blue-800 text-blue-600">Back</a>
		<button onclick="reset()" id="reset" class="ml-2 text-3xl bg-green-200 border font-bold p-4 rounded-lg border-green-800 text-green-600">Pick Another</button>
	</div>

	<style>

		body {
			background-color: #e5dbdb;
			background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ac9292' fill-opacity='0.4'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E");
		}

		.ticket {
			outline: 8px dotted #f7fafc;
    		outline-offset: -4px;
			max-width: 800px
		}

		.inner {
			transition: all 1s 1s ease-in-out;
		}

		.winner .inner{
			transform: scale(3.5);
			box-shadow: 0 0px 25px 5px rgba(0,0,0,0.3);
			color: #b7791f;
			border-color: #b7791f;
			background-color: #ecc94b;
		}

		#overlay {
			position: fixed;
			background: rgba(0,0,0,0.4);
			top:0;
			left:0;
			width:100%;
			height:100%;
		}

		#next {
			position: fixed;
			bottom: 0px;
		}

		#confetti-canvas {
			position: fixed!important;
			top: 0;
		}
	</style>
	<script>
		const actions = document.getElementById("next");
		const button = document.getElementById("overlay");
		const products = document.getElementsByClassName('ticket');
		const product = {{$product->id}};
		let winner = null;

		function setCookie(cName, cValue, expDays) {
			let date = new Date();
			date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = cName + "=" + cValue + "; " + expires + "; path=/";
		}

		function getCookie(cName) {
			const name = cName + "=";
			const cDecoded = decodeURIComponent(document.cookie); //to be careful
			const cArr = cDecoded .split('; ');
			let res;
			cArr.forEach(val => {
				if (val.indexOf(name) === 0) res = val.substring(name.length);
			})
			return res;
		}

		function clear(){
			localStorage.setItem(product,'');
		}

		function reset(){
			products[winner].classList.remove('winner');
			stopConfetti();
			actions.classList.add('hidden');

			setTimeout(() => button.classList.remove('hidden'), 2000);

			winner = null;
		}

		function setWinner(name){
			const existing = getCookie(product);
			console.log(existing);
			if(!existing){
				setCookie(product, name,1);
			}else{
				setCookie(product, `${existing}, ${name}`, 1);
			}
		}

		function random(min, max) {
			return Math.round(Math.random() * (max - min) + min);
		}

		function pick(){
			button.classList.add('hidden');
			winner = random(0,products.length-1);
			products[winner].classList.add('winner');

			setWinner(products[winner].dataset.name);

			products[winner].scrollIntoView({
				behavior: "smooth", block: "center"
			});

			setTimeout(() => startConfetti(),1500);
			setTimeout(() => actions.classList.remove('hidden'),2000);

		}
	</script>
@stop



