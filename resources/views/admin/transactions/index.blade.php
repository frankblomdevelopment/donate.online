@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 ">
        @if (session('status'))
            <div class="text-sm border rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
       @endif
        <div class="w-full mx-auto ">
            <div class="w-full mb-3 flex">
                <form method="get" class="w-full">
                    <select
                            onchange="if(this.value != 0) { this.form.submit(); }"
                            name="range" class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-3 px-2 rounded text-gray-800 font-bold">
                        @foreach ($ranges as $r)
                            <option value="{{ $r }}" @selected($range == $r)>
                                @lang('filters.'.$r)
                            </option>
                        @endforeach

                    </select>
                    <span class="ml-2">{{ $transactions->count() }} Transactions</span>
                </form>

                <a class="ml-auto bg-blue-500 hover:bg-blue-400 text-white border border-blue-600 inline-block py-4 px-5 rounded" href="{{route('transactions.export',['range'=>$range])}}">Export</a>
            </div>

            <div class="overflow-x-auto w-full bg-white border rounded shadow-md ">
                <table class="w-full">
                    <tr class="bg-gray-100 border-b-2">
                        <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left" width="15%">Amount</th>
                        <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left">Name</th>
                        <th class="px-0 py-3 uppercase text-sm text-gray-500 text-left" >Description</th>
                        <th class="px-6 py-3 uppercase text-sm text-gray-500 text-left">Link</th>
                        <th class="px-6 py-3 uppercase text-sm text-gray-500 text-right">Date</th>
                        <th></th>
                    </tr>
                    @forelse($transactions as $transaction)
                        <tr class="border-b">
                            <td class="px-6 py-4 font-bold">${{number_format($transaction->amount,2,'.',',')}}</td>
                            <td class="px-6 py-4 text-green-600">{{($transaction->customer->name ?? '' )}}</td>
                            <td class="px-0 py-4">{{ $transaction->description }}</td>
                            <td class="px-0 py-4">{{ $transaction->link->title }}</td>
                            <td class="px-6 py-4 text-right text-gray-600" title="{{$transaction->created_at->timezone('America/Los_Angeles')->diffForHumans()}}">{{$transaction->created_at->timezone('America/Los_Angeles')->format('d M H:i')}}</td>
                            <td>
                                <a href="{{route('transactions.status.toggle', $transaction)}}" class="px-2 py-1">
                                    @if($transaction->imported)
                                        ✅
                                    @else
                                        🚫
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" class="py-6 with-full text-center text-gray-500">No transactions</td></tr>
                    @endforelse
                </table>
            </div>
        </div>
@endsection