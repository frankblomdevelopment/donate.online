@foreach($data as $key => $value )
    <div class="w-full text-center xl:even:border-l border-0 pt-3 xl:pt-0">
        <h2 class="text-3xl font-medium text-gray-700 pb-2">{{ $value }}</h2>
        <span class="w-full text-gray-500">{{$key}}</span>
    </div>
@endforeach