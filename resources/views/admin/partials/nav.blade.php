<nav class="bg-blue-900 shadow mb-3 py-6">
    <div class="container mx-auto px-4">
        <div class="flex items-center justify-center">
            <div class="mr-6">
                <a href="{{ route('links.index') }}" class="text-lg font-semibold text-gray-100 no-underline">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
            @auth
            <div class="flex-1 ">
                <a href="{{ route('links.index') }}" class="block float-left px-6 text-gray-500 hover:text-white">Links</a>
                <a  href="{{ route('transactions.index') }}" class="block float-left px-6 text-gray-500 hover:text-white">Transactions</a>
                <a  href="{{ route('gateways.index') }}" class="block float-left px-6 text-gray-500 hover:text-white">Integrations</a>
            </div>
            @endauth
            <div class="flex-1 text-right">
                @guest
                    <a class="no-underline hover:underline text-gray-300 text-sm p-3" href="{{ route('login') }}">{{ __('Login') }}</a>
                @else
                    <span class="text-gray-300 text-sm pr-4">{{ Auth::user()->name }}</span>

                    <a href="{{ route('logout') }}"
                       class="no-underline hover:underline text-gray-300 text-sm p-3"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                        {{ csrf_field() }}
                    </form>
                @endguest
            </div>
        </div>
    </div>
</nav>
