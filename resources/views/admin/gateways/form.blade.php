@extends('layouts.app')

@section('content')
    <div class="flex items-center">
        <div class="w-full sm:w-full px-4 md:w-3/4 md:mx-auto md:p-0 lg:w-2/3 max-w-5xl">

            @if (session('status'))
                <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="flex justify-end justify-between mb-5">
                <h1 class="font-medium text-gray-500 pt-4">
                    @if(isset($gateway))
                        Edit Integration
                    @else
                        Add Integration
                    @endif
                </h1>
            </div>
            <div class="flex flex-col break-words bg-white border border-2 rounded shadow-md">
                <form method="post" action="{{(isset($gateway) ? route('gateways.update', $gateway) : route('gateways.store') )}}" class="px-6 py-10">
                    @if(isset($gateway)) @method('put') @endif
                    @csrf
                    <div class="mb-6">
                        <span class="font-medium w-full block mb-2">Title</span>
                        <input value="@if(isset($gateway)){{$gateway->name}}@endif" class="text-xl border p-2 block w-full rounded bg-gray-100" type="text" name="name" placeholder="What is this integration called?">
                        @error('title')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                     <div class="mb-6">
                        <span class="font-medium w-full block mb-2">Account type</span>
                        @foreach($types as $type)
                            <label class="inline-flex cursor-pointer block w-full py-2 border-gray-200">
                                <input type="radio" class="form-radio" name="type" value="{{$type}}" @if(isset($gateway) && $gateway->type === $type) checked="checked" @endif>
                                <span class="ml-2">{{$type}}</span>
                            </label>
                        @endforeach
                        @error('type')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                    <div class="mb-6">
                        <span class="font-medium w-full block mb-2">Public Key</span>
                        <textarea class="text-xl border p-2 block w-full rounded bg-gray-100" name="public_key">@if(isset($gateway)){{$gateway->public_key}}@endif</textarea>
                        @error('public_key')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div> 
                    <div class="mb-12">
                        <span class="font-medium w-full block mb-2">Private Key</span>
                        <textarea class="text-xl border p-2 block w-full rounded bg-gray-100" name="private_key">@if(isset($gateway)){{$gateway->private_key}}@endif</textarea>
                        @error('private_key')  <div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>

                   
                    <button class="bg-blue-500 hover:bg-blue-400 text-white border border-blue-600 text-white py-3 px-4 rounded inline-block float-right" type="submit">@if(isset($gateway)) Save @else Create @endif</button>

                    <a href="{{route('gateways.index')}}" class="float-right py-3 mx-4 text-gray-700">Cancel</a>
                </form>
            </div>

            @if(isset($gateway))
                <form method="POST" action="{{route('gateways.destroy',$gateway)}}" class="bg-white w-full border border-2 shadow-md rounded mt-6 px-6 py-10 flex items-center">
                    @csrf
                    @method('DELETE')
                    <p class="text-lg">Archive this Integration</p>
                    <button type="submit" class="ml-auto bg-red-200 hover:bg-red-100 border border-red-400 inline-block py-4 px-5 rounded text-red-800">Delete</button>
                </form>
            @endif
        </div>
    </div>
@endsection
