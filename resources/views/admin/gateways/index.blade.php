@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        @if (session('status'))
            <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <div class="w-full block float-left relative">
           <div class="absolute w-full flex justify-between">
                <h1 class="font-medium text-gray-500 pt-4">
                    Integrations
                </h1>
                <a class="bg-blue-500 hover:bg-blue-400 text-white border border-blue-600 inline-block py-4 px-5 rounded" href="{{route('gateways.create')}}">Add Integration</a>
            </div>
            <table class="bg-white border rounded shadow-md py-6 mt-16 w-full">
                <tr class="bg-gray-100 border-b-2">
                    <th class="px-6 py-4 uppercase text-sm text-gray-500 text-left">Name</th>
                    <th class="uppercase text-sm text-gray-500 text-left">Last changed</th>
                    <th class=""></th>
                </tr>

            @foreach($gateways as $gateway)
                <tr class="border-b">
                    <td class="px-6 py-6">{{$gateway->name}}</td>
                    <td class="py-6 font-bold ">{{$gateway->updated_at->diffForHumans()}}</td>
                    <td class="text-right pr-2">
                        <a href="{{route('gateways.edit',$gateway)}}" class="bg-gray-200 hover:bg-gray-100 border border-gray-400 inline-block py-3 px-4 rounded text-gray-800">Edit</a>
                    </td>
                </tr>
            @endforeach
            </table>
        </div>
    </div>
@endsection
