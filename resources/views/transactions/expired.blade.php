@extends('layouts.public')

@section('content')
    <div class="flex items-center mt-24">
        <div class="w-full sm:w-full md:w-1/2 md:mx-auto md:p-0 lg:w-1/2 max-w-2xl">
            <div class="flex flex-col break-words bg-white sm:border sm:border-2 rounded shadow-md p-12">
                <h1 class="text-2xl mb-8 text-center">Oops, this link expired</h1>
	            
                	<div class="rounded-full mx-auto bg-red-200 w-32 h-32">
		            	<svg class="fill-current text-red-500 w-12 h-12 mt-10 mx-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
	                	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"></svg>
		            		<path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/>
		            	</svg>
		            </div>
{{--                	<p class="w-full text-center text-lg mt-10 text-red-600 leading-loose"><b>Oops</b> The payment could not be made.<br>Try again later</p>--}}
            </div>
        </div>
    </div>
@endsection
