<html>
<head>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{$transaction->gateway->public_key}}');
        stripe.redirectToCheckout({
            sessionId: "{{$transaction->external_reference}}"
        });
    </script>
</head>
</html>
