@php
$color = "";
switch ($transaction->status) {
	case 'canceled':
		$color = "bg-orange-200 text-orange-600";
		break;
	case 'pending':
		$color = "bg-blue-200 text-blue-600";
		break;
	case 'authorized':
		$color = "bg-blue-200 text-blue-600";
		break;
	case 'expired':
		$color = "bg-red-200 text-red-600";
		break;
	case 'failed':
		$color = "bg-red-200 text-red-600";
		break;
	case 'paid':
		$color = "bg-green-200 text-green-600";
		break;
	default:
		$color = "bg-gray-200 text-gray-600";
		break;
}
@endphp
<span class="rounded font-medium text-sm capitalize px-2 py-1 {{ $color }}">{{$transaction->status}}</span>