@extends('layouts.public')

@section('content')
    <div class="flex items-center mt-6 sm:mt-16 md:mt-20 lg:mt-24">
        <div class="w-full sm:w-full md:w-2/3 mx-auto md:p-0 lg:w-1/2 max-w-2xl">
            <div class="flex flex-col break-words bg-white sm:border sm:border-2 rounded shadow-md p-6 sm:px-12 sm:py-8">
                <h1 class="text-2xl sm:text-3xl md:text-4xl mb-8 text-center leading-normal">{{ $link->title }}</h1>
                @if(count($transactions) == 0)
                    <h2 class="mb-8 text-xl text-gray-600 w-full text-center">You have not made any transactions yet.</h2>
                @elseif($link->hasProducts())
                    <h2 class="mb-8 text-xl font-bold">Tickets bought for:</h2>
                @else
                    <p class="w-full text-xl block mb-8" >Payments made by <span class="font-bold">{{$customer->name}}</span>, </p>
                @endif

                @foreach($transactions as $transaction)
                    <div class="w-full mb-8">
                        <div class=" flex items-start w-full mb-1">
                            @if($link->hasProducts() )
                            <p class="text-2xl w-full" >{{$transaction->name}}</p>
                            @else
                                <div class="flex flex-wrap w-full">
                                    <p class="text-2xl w-full" >${{number_format($transaction->amount,2,'.',',')}}</p>
                                    <p class="py-2 text-md text-gray-500">{{$transaction->description}}</p>
                                </div>

                            @endif
                            <span class="text-sm text-gray-600">{{$transaction->created_at->setTimezone('America/Los_Angeles')->format('m/d/Y')}}</span>
                        </div>

                        @foreach($transaction->products as $product)
                            <p class="w-full py-1 text-lg pl-4">
                                <b>{{$product->pivot->amount}} &times;</b> {{$product->name}}
                            </p>
                        @endforeach
                    </div>
                @endforeach

                <div class="mb-10 mt-4">
                    <a href="{{route('transaction.create',$link)}}" class="text-blue-500 hover:underline text-lg">Back to the link</a>
                </div>
        </div>
    </div>
@endsection

@section('script')
<style >
    #submitButton[disabled=disabled], #submitButton:disabled {
        opacity: 0.2!important;
        cursor: not-allowed;
    }
</style>
<script >
    const submitButton = document.getElementById("submitButton");
    const formatter = new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' });

    function input(){
        const products = document.getElementsByClassName('product');
        let total = 0;

        if(!products.length) return;
        
        for(let prod of products){
            const price = Number(prod.dataset.amount);
            const count = Math.round(Number(prod.value));
            if(count < 1) {
                continue;
            }
            const amount = Math.round((price * count) * 100) / 100
            total = Math.round((total + amount) * 100 ) / 100;
        }

        if(total < 0.5){
            submitButton.innerText = "Pay";
            submitButton.disabled = true;
        }else{
            submitButton.innerText = `Pay ${formatter.format(total)}`;
            submitButton.disabled = false;
        }
    }

    input();

</script>

@endsection