@extends('layouts.public')

@section('content')
    <div class="flex items-center mt-6 sm:mt-16 md:mt-20 lg:mt-24">
        <form class="w-full sm:w-full md:w-2/3 mx-auto md:p-0 lg:w-1/2 max-w-2xl" method="post" action="{{route('transaction.store', $link->code)}}">
            <div class="flex flex-col break-words bg-white sm:border-2 rounded shadow-md p-6 sm:px-12 sm:py-8">
                <h1 class="text-2xl sm:text-3xl md:text-4xl mb-8 text-center leading-normal">{{ $link->title }}</h1>
                @if($link->show_status)
                    <div class="flex flex-wrap xl:flex-no-wrap mb-12">
                        @include('admin.partials.infoblock',['data' => ['Raised' => "$". $link->raised, 'Transactions' => $link->transactions()->paid()->count()]])
                    </div>
                @endif
                <p class="w-full text-xl block mb-8" >Hi <span class="font-bold">{{$customer->name}}</span>, </p>
                @csrf
                @if($link->gateways->count() > 1)
                <div class="mb-6">
                    <span class="font-medium w-full block mb-2">Choose the recipient</span>
                    <div class="border rounded-lg border-gray-300"> 
                        @foreach($link->gateways as $key => $gateway)
                        <label class="px-5 py-4 block overflow-hidden width-full cursor-pointer border-gray-300 border-b last:border-b-0">
                            <input type="radio" class="form-radio block float-left h-8 w-8" name="gateway" value="{{$gateway->id}}" @if(sizeof($link->gateways) == 1) checked="checked" @endif>
                            <span class="ml-4 pt-1 block float-left text-xl">{{ $gateway->name }}</span>
                        </label>
                        @endforeach
                    </div>
                    @error('gateway')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                </div>
                @endif

                @if($link->with_tickets)
                    <div class="mb-4">
                        <span class="font-medium w-full block mb-2">Purchase for</span>
                        <div class="w-full flex relative">
                            <input type="text" name="name" class="p-4 text-2xl text-gray-800 rounded-lg border border-gray-500 block w-full outline-none" placeholder="This can be any name you want" value="{{old('name',$customer->name)}}">
                        </div>
                        @error('name')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>
                    <div class="mb-10">
                        <a href="{{route('transaction.history',$link)}}" class="text-blue-500 hover:underline text-lg">See your purchases</a>
                    </div>
                @endif

                @if(!$link->hasProducts() )
                    @if($link->price_type == "FIXED")
                        <div class="mb-6" >
                            <span class="font-medium w-full block mb-2">Amount</span>
                            <div class="w-full block relative">
                                <span class="text-4xl text-gray-500 font-mono absolute pt-4 px-5">$</span>
                                <input type="hidden" name="amount" value="{{$link->price}}">
                                <div class="pl-12 py-4 text-4xl text-gray-600 bg-gray-200 font-mono tracking-tighter rounded-lg border block w-full outline-none">{{$link->price}}</div>
                            </div>

                            @error('amount')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                        </div>
                    @else
                        <div class="mb-6">
                            <span class="font-medium w-full block mb-2">Choose your amount</span>
                            <div class="w-full block relative">
                                <span class="text-4xl text-gray-500 font-mono absolute pt-5 px-5">$</span>
                                <input type="text" name="amount" class="pl-12 py-4 text-4xl text-gray-600 font-mono tracking-tighter rounded-lg border block w-full outline-none" value="{{old('amount')}}">
                            </div>

                            @error('amount')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                        </div>
                    @endif
                <div class="mb-6">
                    <span class="font-medium w-full block mb-2">Description</span>
                    @if($link->description_type != "fixed") 
                        <textarea rows="3" name="description" class="px-5 py-4 text-gray-800 text-xl tracking-tight rounded-lg border block w-full outline-none" placeholder="Payment for . . .">{{ old('description',$link->description) }}</textarea>
                    @else
                        <div class="bg-gray-200 px-5 py-4 text-gray-800 text-xl tracking-tight rounded-lg border block w-full outline-none">{{ $link->description }}</div>
                        <div class="text-gray-500 alert alert-danger mt-2">You will see this description on your transaction.</div>
                    @endif
                    @error('description')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                </div>
                @else
                <div class="mb-6">
                    <span class="font-medium w-full block mb-4">Select from following options:</span>
                    @foreach ($link->products as $product )
                        <div class="w-full mb-6">
                            @if(!is_null($product->image))
                            <img alt="image" src="{{$product->image}}"  class="mb-2 mt-10" />
                            @endif
                            <div class="w-full flex relative items-center">
                                <div class="flex w-full flex-wrap">
                                <h2 class="w-full text-xl md:text-2xl break-all py-3">{{$product->name}}
                                    @if($product->hasLimitedAvailability() && $product->isAvailable())
                                        <span class="block leading-normal text-gray-600 text-base">
                                            <b>{{$product->remaining_tickets}}</b> Tickets Remaining
                                        </span>
                                    @endif
                                    @if($product->multiplier > 1)
                                    <span class="block leading-normal text-green-600 text-xl">
                                        <b>&times;{{$product->multiplier}}</b> Ticket Multiplier
                                    </span>
                                    @endif
                                </h2>
                                    <small class="w-full text-gray-600 text-base mb-2">{{$product->description}}</small>
                                </div>
                                @if($product->multiplier > 0)
                                    @if($product->isAvailable())
                                        <div class="flex items-center">
                                            <span class="block w-16 md:w-32 text-right text-lg md:text-2xl text-gray-500 pr-3">${{$product->price}} x</span>
                                            <input oninput="input()" data-amount="{{ $product->price }}"
                                           inputmode="numeric"
                                            type="number"
                                            min="0"
                                            step="1"
                                            max="{{$product->hasLimitedAvailability() ? $product->remainingTickets : ''}}"

                                            @if(!$product->isAvailable())
                                             disabled="disabled"
                                             @endif

                                            name="products[{{$product->id}}][amount]"
                                            placeholder="0"
                                            value="{{old('products.'.$product->id . '.amount')}}"
                                            class="product p-3 text-md sm:text-xl md:text-2xl text-gray-600 font-mono tracking-tighter rounded-lg border block w-16 md:w-24 outline-none border-gray-500">
                                        </div>
                                    @else
                                        <div class="flex items-center">
                                            <span class="block w-32 text-right text-xl md:text-2xl text-red-800">Sold Out</span>
                                        </div>
                                    @endif
                                @endif

                            </div>
                            @error('products.'.$product->id)<div class="text-red-500 alert alert-danger mb-2 text-right">{{ $message }}</div> @enderror
                        </div>


                    @endforeach
                    @error('amount')<div class="text-red-500 alert alert-danger mt-2 text-lg">{{ $message }}</div> @enderror
                </div>
                @endif
                @if($link->with_tip)
                    <div class="mb-12">
                        <span class="font-medium w-full block mb-2">Add a donation</span>
                        <div class="w-full flex relative">
                            <span class="text-4xl text-gray-500 font-mono absolute pt-5 px-5">$</span>
                            <input type="number"  inputmode="decimal" oninput="input()" id="tip" step="0.1" name="tip" class="pl-12 py-4 text-4xl text-gray-600 font-mono tracking-tighter rounded-lg border border-gray-500 block w-full outline-none" value="{{old('tip')}}">
                        </div>
                        @error('tip')<div class="text-red-500 alert alert-danger mt-2">{{ $message }}</div> @enderror
                    </div>
                @endif
                <button type="submit" id="submitButton" class="bg-blue-500 hover:bg-blue-400 border border-blue-600 inline-block py-4 text-3xl text-center rounded text-white">Pay</button>
                @if($link->fee)
                    <div class="text-gray-500 text-base mt-2 text-center">A {{$link->fee}}% transaction fee will be added.</div>
                @endif

                @if(!$link->with_tickets)
                    <div class="mt-12 text-center">
                        <a href="{{route('transaction.history',$link)}}" class="text-blue-500 hover:underline text-lg">See previous payments for {{$link->title}}</a>
                    </div>
                @endif
            </div>
        </form>
    </div>
@endsection

@section('script')
<style >
    #submitButton[disabled=disabled], #submitButton:disabled {
        opacity: 0.2!important;
        cursor: not-allowed;
    }
</style>
<script >
    const submitButton = document.getElementById("submitButton");
    const formatter = new Intl.NumberFormat('us-US', { style: 'currency', currency: 'USD' });

    function input(){
        const products = document.getElementsByClassName('product');
        let total = 0;

        if(!products.length) return;
        
        for(let prod of products){
            const price = Number(prod.dataset.amount);
            const count = Math.round(Number(prod.value));
            if(count < 1) {
                continue;
            }
            const amount = Math.round((price * count) * 100) / 100
            total = Math.round((total + amount) * 100 ) / 100;
        }

        let tip = 0;
        const tipInput = document.getElementById("tip");
        if(tipInput && !isNaN(tipInput.value)){
            const value = Number(tipInput.value);
            if(value> 0){
                tip = value;
            }
        }

        if(total + tip < 0.5){
            submitButton.innerText = "Pay";
            submitButton.disabled = true;
        }else{
            submitButton.innerText = `Pay ${formatter.format(total+ tip)}`;
            submitButton.disabled = false;
        }
    }

    input();

</script>

@endsection