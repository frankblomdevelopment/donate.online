<?php


return [
    'l30'  => 'Last 30 Days',
    'year' => 'This Year',
    'last_year' => 'Last Year',
    'quarter' => 'This Quarter',
    'last_quarter' => 'Last Quarter',
    'month' => 'This Month',
    'last_month' => 'Last Month',
    'week' => 'This Week',
    'last_week' => 'Last Week',
    'today' => 'Today',
];
