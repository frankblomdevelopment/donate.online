<?php

use Illuminate\Support\Facades\Route;

Route::get('auth', 'Auth\SsoController@handleProviderCallback');
Route::post('logout', 'Auth\SsoController@logout')->name('logout');

Route::redirect('/', '/links');

Route::group(['middleware' => 'auth'], function(){
    Route::resource('links', 'LinkController');
    Route::resource('gateways', 'GatewayController');

    Route::get('transactions', 'TransactionController@index')->name('transactions.index');
    Route::get('transactions/export', 'TransactionController@export')->name('transactions.export');

    Route::get('links/{link}/export', 'LinkController@export')->name('links.export');
    Route::get('links/{link}/products/export', 'LinkController@exportProducts')->name('links.products.export');
    Route::get('links/{link}/dashboard', 'LinkController@dashboard')->name('links.dashboard');

    Route::get('/products/{product}/pick', 'ProductController@pick')->name('product.pick');

    Route::get('/transactions/{transaction}/status/toggle','TransactionStatusController')->name('transactions.status.toggle');
});

Route::group(['middleware' => ['web','isCustomer']], function(){
    // public link to start a transaction
    Route::get('{link}', 'TransactionController@create')->name('transaction.create');
    Route::post('{link}', 'TransactionController@store')->name('transaction.store');
    Route::get('{link}/history', 'TransactionController@history')->name('transaction.history');

    // payment page for stripe payments
    Route::get('pay/{transaction}', 'PaymentController@create')->name('payment.create');
    Route::post('pay/{transaction}', 'PaymentController@charge')->name('payment.charge');
});


// error link
Route::get('t/{transaction}/error', 'TransactionController@error')->name('transaction.error');
// after payment return link
Route::get('t/{transaction}','TransactionController@show')->name('payment-return-url');

// webhook
Route::post('t/{provider}','TransactionController@update')->name('webhook-url');

