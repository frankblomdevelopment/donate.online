module.exports = {
  theme: {
    extend: {},
  },
  variants: {
    borderWidth: ["responsive", "last", "even"],
    margin: ["responsive", "last"],
    padding: ["responsive", "last"],
    border: ['color']
  },
  plugins: [require("@tailwindcss/custom-forms")],
};
